const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
    // the data will be received from the registration form when the user logs in, a token will be created with user's information
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }
    // generate a json web token using the jwt's sign method
    // generates the token using the form data, and the secret code with no additional options provided. 
    return jwt.sign(data, secret, {})


}



// authorisation - token
module.exports.verify = (req, res, next) => {
    // the token is retreived from the request header
    let token = req.headers.authorization;
    // token received and not undefined
    if(typeof token !== "undefined") {
        console.log(token)
        // the token sent is a type of bearer token which recieved a contains the "bearer as a prefix"
        // syntax: string.slice(start, end)
        token = token.slice(7, token.length) // the purpose of slice is so that it will not include the bearer. bearer 23454jl;q23j4l;2 (bear+space is 7 paces)

        return jwt.verify(token, secret, (err, data) => {
            if (err) {
                return res.send({auth: "failed"})
            } else {
                next() // invokes our controller functions
            }
        })
    } else {
        return res.send({auth: "failed"})
    }
}


// decode

module.exports.decode = (token) => {
    
    if(typeof token !== "undefined") {
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {
            if(err) {
                return null
            } else {
                return jwt.decode(token, {complete:true}).payload
            }
        })
        
    } else {
        return null
    }
}