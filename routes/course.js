const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// router.post("/", auth.verify,(req, res) => {

//     courseController.addCourse(req.body).then(resultFromController =>
//         res.send(resultFromController))

//     })

// failed activity

// router.post("/", auth.verify,(req, res) => {
//     const userData = auth.decode(req.headers.authorization)
    
//         if(userData.isAdmin == false) {
//             return false
           
//         } else {

//            return true
            // courseController.addCourse(req.body).then(resultFromController =>
            //     res.send(resultFromController))
 
          
//         }
 
//         courseController.addCourse(req.body).then(resultFromController =>
//             res.send(resultFromController))
  


  
// })


//Activity Solution 1

router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		courseController.addCourse(req.body).then(resultFromController => 
		res.send(resultFromController))
	} else {
		res.send("Not Authorized")
	}	
})


//Activity Solution 2


// router.post("/", auth.verify, (req, res) => {		
// 	const userData = auth.decode(req.headers.authorization);

// 	courseController.addCourse(userData, req.body).then(resultFromController => 
// 		res.send(resultFromController))
// });



// retrieve all the courses

router.get("/all", (req, res) => {
    courseController.getAllCourses().then(resultFromController =>
        res.send(resultFromController))

})

// retreive all active course

router.get("/", (req,res) => {
  courseController.getAllActive().then(resultFromController =>
     res.send(resultFromController))
})


router.get("/:courseId", (req,res) => {
    console.log(req.params)
    console.log(req.params.courseId) // request.params means you are requesting from url

    courseController.getCourse(req.params).then(resultFromController =>
        res.send(resultFromController))
})

// update a course 

router.put("/:courseId", auth.verify, (req,res) => {
    courseController.updateCourse(req.params, req.body).then(resultFromController =>
        res.send(resultFromController))
})

// activity - acrhiving

router.put('/:id/archive', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin === true) {
    courseController.archiveCourse(req.params.id, req.body).then(resultFromController => 
        res.send(resultFromController));
    } else {
		res.send("Not Authorized");
	}	
});



module.exports = router;