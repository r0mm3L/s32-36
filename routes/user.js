const express = require("express");
const router = express.Router();
const userController = require("../controllers/user")
const auth = require("../auth");


router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => 
        res.send(resultFromController))
})


router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => 
        res.send(resultFromController))
})

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => 
        res.send(resultFromController));
})

// router.get("/details", auth.verify, (req, res) => {

//     const userData = auth.decode(req.headers.authorization)

//     userController.getProfile({userId: req.body.id}).then(resultFromController => 
//         res.send(resultFromController));
// })

// router.post("/details", (req, res) => {
// 	userController.getProfile({userId: req.body.id}).then(resultFromController => 
// 		res.send(resultFromController))
// })

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
    console.log(req.headers)
	userController.getProfile({userId: req.body.id}).then(resultFromController => 
		res.send(resultFromController))
})

//for enrolllinig a user

// router.post("/enroll", (req, res) => {
    
//     let data = {
//         userId: req.body.userId,
//         courseId: req.body.courseId
//     }

//     userController.enroll(data).then(resultFromController =>
//         res.send(resultFromController));
// })



// enroll copy from teacher


// router.post("/enroll", (req, res) => {

// 	let data = {
// 		userId: req.body.userId,
// 		courseId: req.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController => 
// 		res.send(resultFromController));

// });

/* 

activity

*/

router.post("/enroll", auth.verify,(req, res) => {

    const userData = auth.decode(req.headers.authorization);

	let data = {
		// userId: req.body.userId,
        userId: userData.id, // so that only the logged user is allowed to enrol for himself/herself
		courseId: req.body.courseId
	}

    if(userData.isAdmin === false) {
        // if(userData.isAdmin === false && userData.userId === data.userId) {
        // if(userData.userId === data.userId) {
	userController.enroll(data).then(resultFromController => 
		res.send(resultFromController));
    } else {
		res.send("Not Authorized");
	}	

});



module.exports = router;