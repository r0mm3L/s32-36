const express = require('express');


const mongoose = require('mongoose');
const cors = require('cors')

const userRoutes = require("./routes/user")
const courseRoutes = require("./routes/course")


const app = express();


const port = process.env.PORT || 4000;


app.use(express.json())
app.use(express.urlencoded({extended: true}));
app.use(cors())

mongoose.connect('mongodb+srv://admin:admin123@zuitt-bootcamp.cpkli.mongodb.net/course-booking?retryWrites=true&w=majority', {

        useNewUrlParser: true,
        useUnifiedTopology: true
    });
let db = mongoose.connection;

 
    db.on('error', console.error.bind(console, "connection error"))
 
    db.once('open', () => console.log('Connected to the cloud database'))

    
    app.use("/users", userRoutes);
    app.use("/course", courseRoutes);

    app.listen(port, () => console.log(`The Server is running at port ${port}`));