const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// CHECKED EMAIL
module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {

        if(result.length > 0 ){
            return True
        } else {
            return false
        }
    })


}

//REGISTRATION
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)

    })

    return newUser.save().then((user, error) => {
        if(error) {
            return false

        } else {
            return true
        }
    })
}


// login 

module.exports.loginUser = (reqBody => {
    return User.findOne({email: reqBody.email}).then(result => {

        if(result == null) {
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect) {
                return {access: auth.createAccessToken(result)}
            } else {
                return false
            }
        }
    })
})


// acitivity

// module.exports.getProfile = (reqBody) => {
//     return User.find({id: reqBody.id}).then(result => {

      
//         let newPassword = "";
//         result.password = newPassword.password
//         return result.save().then((newPass, err) => {
//             if(err) {
//                 return false
    
//             } else {
//                 return newPass
//             }
       
//         })
        
//     })



// }


// acitivity
// module.exports.getProfile = (reqBody) => {
//     return User.find({id: reqBody.body}).then(result => {

//         let newUser = ({
//             _id: "6181253b2555aeebd269ef97",
//             firstName: "Rommel2",
//             lastName: "Alastra2",
//             email: "rommelalastra2@mail.com",
//             password: "",
//             isAdmin: false,
//             mobileNo: "091714344",
//             enrollments: [],
//             __v:0
    
//         })

//         return newUser
//     })


// }

// acitivity
// module.exports.getProfile = (reqBody) => {
//     return User.find({body: reqBody.body}).then(result => {
//         // return User.find(body).then(result => {
       
//         let newUser = ({
//             _id: reqBody.id,
//             firstName: firstName,
//             lastName: reqBody.lastName,
//             email: reqBody.email,
//             password: "",
//             isAdmin: reqBody.isAdmin,
//             mobileNo: reqBody.mobileNo,
//             enrollments: reqBody.enrollments,
//             __v:0
    
//         })

//         return newUser
//     })


// }


// module.exports.getProfile = (data) => {
//     return User.find(data.userId).then(result => {
//         // return User.find(body).then(result => {
       
//         result.password = "";

//         return result
//     })


// }



// Retrieve user details - answer to activity
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		result.password = "";
		return result;

	});

};


// for enrolling user
// async await will be usseed in eenrollling the user because we willl need to update 2 separate documents when enrolling a usesr
// module.exports.enroll = async (data) => {

// let isUserUpdated = await User.findById(data.userId).then( user => {

//     user.enrollments.push({courseId: data.courseId});

//     return user.save().then((user, error) => {
//         if(error) {
//             return false
//         } else {
//             return true
//         }
//     })
// })

// let isCourseUpdated = await Course.findById(data.courseId).then(course => {

//     course.enrollees.push({userId: data.userId});

//     return course.save().then((course, error) = {
//         if(error) {
//             return false
//         } else {
//             return True
//         }
//     })
// })

// if(isUserUpdated && isCourseUpdated) {
//     return true
// } else {

//     return false

// }

// }


/* 

ENROLLMENT COPIED BY TEACHER


*/

//Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user.
module.exports.enroll = async (data) => {

//Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back
	let isUserUpdated = await User.findById(data.userId).then( user =>{

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})

//Using the "await" keyword will allow the enroll method to complete the course before returning a response back
	let isCourseUpdated = await Course.findById(data.courseId).then( course => {
		//Adds the userId in the course's enrollees array
		course.enrollees.push({userId: data.userId});
		//save the updated course information information
		return course.save().then((course, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})

//Condition that will check if the user and course documents have been updated
		//User enrollment is successful
		if(isUserUpdated && isCourseUpdated) {
			return true
		} 

		//User enrollment failure
		else {
			return false
		}

}

