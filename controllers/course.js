const Course = require("../models/Course");
const auth = require("../auth");

// module.exports.addCourse = (reqBody) => {
//     let newCourse = new Course({
//         name: reqBody.name,
//         description:  reqBody.description,
//         price: reqBody.price
//     })
    
//     return newCourse.save().then((course, error) => {
//         if(error) {
//             return false
//         } else {
//             return true
//         }
//     })
// }


// add course not activity

module.exports.addCourse = (reqBody) => {
    let newCourse = new Course({
        name: reqBody.name,
        description:  reqBody.description,
        price: reqBody.price
    })
    
 

    return newCourse.save().then((course, error) => {
        if(error) {
            return false
        } else {
            return true
        }
    })
}

//Activity Solution 2


// module.exports.addCourse = async (user, reqBody) => {
// 	if(user.isAdmin == true){
// 		let newCourse = new Course({
// 			name: reqBody.name,
// 			description: reqBody.description,
// 			price: reqBody.price
// 		})

// 		return newCourse.save().then((course, error) => {
// 			if(error){
// 				return false;
// 			}
// 			else{
// 				return true;
// 			}
// 		})
// 	}
// 	else{
// 		return ("Not Authorized");
// 	}
// }


// retrieve all the courses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    });
};

// retrieve all active courses
module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(result => {
        return result;
    })

}

// retrieve specific course

module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        // return result;
        // return ({name: `${result.name}`})
        let result2 = {
            name: result.name,
            price: result.price

        }

        return result2
    })
}

// update a course

module.exports.updateCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

    // syntax:
        //findByIdandUpdate(document, updatesToBeApplied)
        return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
        .then((course, error) => {
            if(error){
                return false
            } else {
                return true
            }
        })

}


//activity - archiving

module.exports.archiveCourse = (courseId, newBody) => {
    return Course.findById(courseId).then((result) => {


        result.isActive = newBody.isActive
        return result.save().then((NewActiveStatus, sendErr) => {

            if(sendErr){
                console.log(sendErr)
                return false
            } else {
                return true
            }
        })
    })
}


// activity answer 